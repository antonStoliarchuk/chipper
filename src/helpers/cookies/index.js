import Cookies from 'universal-cookie';
import config from '@/config';

const cookies = new Cookies();

cookies.utils = {
    getAuth() {
        return cookies.get(config.cookies.access_token);
    },
    hasAuth() {
        return Boolean(cookies.get(config.cookies.access_token));
    },
    removeAuth() {
        cookies.remove(config.cookies.access_token);
    },
    setAuth(token, keepSignedIn = false) {
        const msDay  = 86400000;
        const msYear = 31556952000;

        const cookie = config.cookies.access_token;
        const maxAge = keepSignedIn ? msYear : msDay;

        cookies.set(cookie, token, { maxAge });
    },
};

export default cookies;
