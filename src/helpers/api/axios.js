import axios from 'axios';
import config from '@/config';
import localStorageService from '@/services/LocalStorage/';
// import cookies from '@/helpers/cookies';


// Create

const instance = axios.create({
    baseURL: config.api.baseUrl,
});

// Interceptors

instance.interceptors.request.use(sendRequestAuthorization);

function sendRequestAuthorization(request) {
    const user = localStorageService.loadUser('user');
    if (user) {
        const token = user.token;

        if (token) {
            request.headers.Authorization = `Bearer ${token}`;
        }
    }

    return request;
}

// function sendRequestAuthorization(request) {

//     if (cookies) {
//         const cookie = config.cookies.access_token;
//         const token  = cookies.get(cookie);

//         if (token) {
//             request.headers.Authorization = `Bearer ${token}`;
//         }
//     }

//     return request;
// }

export default instance;
