import axios from './axios';

class API {

    constructor() {
        this.axios = axios;
        this.get = axios.get;
        this.post = axios.post;
        this.URL = 'http://18.184.210.61/api/';
    }


    async login(data) {
        const response = await this.post(`${this.URL}connectionClient`, data);
        return response;
    }

}

export default new API();
