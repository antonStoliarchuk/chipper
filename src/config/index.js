export default {
    api: {
        baseUrl: process.env.API_URL || 'http://18.184.210.61/api/',
    },
    helmet: {
        title: 'Chippper',
        titleTemplate: 'React Starter - %s',
        htmlAttributes: {
            lang: 'en',
        },
        meta: [
            {
                name: 'description',
                content: 'React starter kit',
            },
        ],
    },
    server: {
        host    : process.env.NODE_HOST || 'localhost',
        port    : process.env.PORT || 3000,
        sslPort : process.env.SSL_PORT || 3001,
    },
};
