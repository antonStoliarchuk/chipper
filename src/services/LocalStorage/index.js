function saveUser(data) {
    console.log('saving data', data);
    localStorage.setItem('user', JSON.stringify(data));
}

function loadUser(user) {
    return JSON.parse(localStorage.getItem(user));
}
export default {
    saveUser,
    loadUser,
};
