import React from 'react';
import { IndexRoute, Route } from 'react-router';
import { RouterMiddleware } from '@/helpers/router/middleware';
import App from '@/modules/core/components/App';
import { Error, NotFound, DashBoardMain, Segment, Client, Redemption, Coupon, Campaign, Report, Branch, PersonalArea } from '@/modules/core/pages';
import Login from '@/modules/core/pages/Auth/Login';
import DistributorAddModal from '@/modules/common/components/DistributorAddModal';
// import { Error, Home, NotFound } from '@/modules/core/pages';

const middleware = new RouterMiddleware();

const routes = (
    <Route onEnter={middleware.onEnter} onChange={middleware.onChange}>
        <Route path="error" component={Error} />
        <Route path="/" component={App}>
            <IndexRoute label="DashboardMain" component={DashBoardMain} />
            <Route path="client" label="client" component={Client} />
            <Route path="segment" label="segment" component={Segment} />
            <Route path="redemption" label="redemption" component={Redemption} />
            <Route path="coupon" label="coupon" component={Coupon} />
            <Route path="campaign" label="campaign" component={Campaign} />
            <Route path="report" label="report" component={Report} />
            <Route path="branch" label="branch" component={Branch} />
            <Route path="login" label="login" component={Login} />
            <Route path="personal-area" label="personal-area" component={PersonalArea} />
            <Route path="popup" component={DistributorAddModal} />
            <Route path="*" component={NotFound} status={404} />
        </Route>
    </Route>
);

export default routes;
export { middleware };
