import React from 'react';

import './style.scss';

const DsitributorAdd = () => {
    return (
        <div className="page-align pad-page">
            <div className="text-center" styleName="modal-padding">
                <h1 className="bold size-24 text-center">סימנו אותך כמפיץ של מוצריהם</h1>
                <h2 className="text-center size-20">אישור היותך מפיץ מקנה ליצרן אפשרות לייצר קופונים למימוש בפלטפורמות המכירה שלך</h2>
                <p className="text-center">Lorem ipsum dolor sit amet consectetur adipisicing elit. Labore molestias eos officiis,
                    obcaecati impedit voluptas nostrum corporis enim fuga laborum, dignissimos nulla facere corrupti,
                    necessitatibus vel suscipit. Ducimus, quam mollitia!Fuga iure nam, ex veritatis harum ipsum illo dolores culpa aliquid ad.
                    Neque, laboriosam magni accusamus voluptatem aut inventore ducimus fuga dolor officia, iure dignissimos deleniti minus quas saepe? Soluta.
                </p>
                <button className="text-center" styleName="approve">אני מאשר</button>
                <p className="text-center" styleName="decline">אני לא מעוניין בקופונים של יצרן זה</p>
            </div>
        </div>
    );
};

export default DsitributorAdd;
