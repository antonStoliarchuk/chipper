import React from 'react';

import './style.scss';

const openDistRequest = ({ name, date }) => {
    console.log('got name and date', name, date);
    return (
        <div styleName="side-padding">
            <div className="flex space-between">
                <div>
                    <img src="https://via.placeholder.com/100" alt="company-logo" />
                </div>
                <div className="flex col align-self-end">
                    <h1 className="bold size-16 mb-15">{name}</h1>
                    <p className="size-14">הוגשה בקשה</p>
                    <p className="size-14">ב-{date}</p>
                </div>
                <div className="flex">
                    <button className="align-self-end ml-20" styleName="not-interested">לא מעוניין</button>
                    <button className="align-self-end" styleName="approve">אני מאשר</button>
                </div>
            </div>
        </div>
    );
};

export default openDistRequest;
