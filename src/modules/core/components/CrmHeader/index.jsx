import React from 'react';
import { Link } from 'react-router';
// import DatePicker from '@/modules/forms/components/InputDatepicker';
import './style.scss';

const Header = () => {
    return (
        <div
            className="page-align flex wrap space-between"
            styleName="header"
        >
            <div className="flex mr-25" styleName="header-input-wrapper">
                <input
                    styleName="input-field"
                    type="text"
                    name="dashboard-search"
                    placeholder="הזנ\י קוד קופון, שם קמפיין, סיטונאי או קמעונאי"
                />
            </div>
            <div className="mr-25">
                <button className="ml-25">
                    <img
                        src="../../../../assets/svg/Asset 34.png"
                        alt="notifications"
                    />
                </button>
                <button styleName="user-btn">
                    <Link to="personal-area">
                        <img
                            styleName="user-icon"
                            src="../../../../assets/svg/Asset 30.png"
                            alt="user"
                        />
                        שלומי משולם
                    </Link>
                </button>
            </div>
        </div>
    );
};

export default Header;
