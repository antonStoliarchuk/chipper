import React, { Component } from 'react';
import Helmet from 'react-helmet';
import { hot } from 'react-hot-loader';

import config from '@/config';

import { ModalProvider } from '@/modules/core/context/modal';
import AppModal from '@/modules/core/components/AppModal';

import NavBar from '@/modules/core/components/NavBar';
import Header from '@/modules/core/components/CrmHeader';

import './styles.scss';

class App extends Component {

    render() {
        const { children } = this.props;
        return (
            <div styleName="main">
                <Header />
                <NavBar />
                <Helmet {...config.helmet} />
                <ModalProvider>
                    <main>{children}</main>
                    <AppModal />
                </ModalProvider>
            </div>
        );
    }

}

export default hot(module)(App);
