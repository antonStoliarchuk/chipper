import React from 'react';
import { Link } from 'react-router';
// import { NavLink } from 'react-router-dom';
import './style.scss';

const NavBar = () => {
    return (
        <aside styleName="side-menu-container">
            <nav>
                <img
                    src="../../../../assets/svg/Asset 35.png"
                    alt="chipper-logo"
                    styleName="chipper-logo"
                />
                <ul>
                    <li>
                        <Link
                            onlyActiveOnIndex
                            to="/"
                            styleName="list-item"
                            activeClassName="active"
                        >
                            <img
                                src="../../../../assets/svg/Asset 37.png"
                                alt="main-dash"
                            />
                            דאשבורד ראשי
                        </Link>
                    </li>
                    <li>
                        <Link
                            to="client"
                            styleName="list-item"
                            activeClassName="active"
                        >
                            <img
                                src="../../../../assets/svg/Asset 39.png"
                                alt="clients"
                            />
                            לקוחות
                        </Link>
                    </li>
                    <li>
                        <Link
                            to="segment"
                            styleName="list-item"
                            activeClassName="active"
                        >
                            <img
                                src="../../../../assets/svg/Asset 41.png"
                                alt="clients"
                            />
                            סגמנטים
                        </Link>
                    </li>
                    <li>
                        <Link
                            to="redemption"
                            styleName="list-item"
                            activeClassName="active"
                        >
                            <img
                                src="../../../../assets/svg/Asset 44.png"
                                alt="redemption"
                            />
                            מימושים
                        </Link>
                    </li>
                    <li>
                        <Link
                            to="coupon"
                            styleName="list-item"
                            activeClassName="active"
                        >
                            <img
                                src="../../../../assets/svg/Asset 46.png"
                                alt="coupons"
                            />
                            קופונים
                        </Link>
                    </li>
                    <li>
                        <Link
                            to="campaign"
                            styleName="list-item"
                            activeClassName="active"
                        >
                            <img
                                src="../../../../assets/svg/Asset 48.png"
                                alt="campaigns"
                            />
                            קמפיינים
                        </Link>
                    </li>
                    <li>
                        <Link
                            to="report"
                            styleName="list-item"
                            activeClassName="active"
                        >
                            <img
                                src="../../../../assets/svg/Asset 50.png"
                                alt="reports"
                            />
                            דו"ח מימושים
                        </Link>
                    </li>
                    <li>
                        <Link
                            to="branch"
                            styleName="list-item"
                            activeClassName="active"
                        >
                            <img
                                src="../../../../assets/svg/Asset 52.png"
                                alt="branchs"
                            />
                            סניפים
                        </Link>
                    </li>
                </ul>
                <button styleName="create-btn">יצירת קופון</button>
            </nav>
            <nav styleName="bottom-menu-items">
                <ul>
                    <li>
                        <Link
                            to="/checkout"
                            styleName="list-item chipper-checkout"
                            activeClassName="active"
                        >
                            <img
                                src="../../../../assets/svg/Asset 54.png"
                                alt="checkout"
                            />
                            חשבון מול Chipper
                        </Link>
                    </li>
                    <li>
                        <Link
                            to="help"
                            styleName="list-item"
                            activeClassName="active"
                        >
                            <img
                                src="../../../../assets/svg/Asset 56.png"
                                alt="chehelpckout"
                            />
                            עזרה
                        </Link>
                    </li>
                    <li>
                        <Link
                            to="/contact"
                            styleName="list-item"
                            activeClassName="active"
                        >
                            <img
                                src="../../../../assets/svg/Asset 58.png"
                                alt="contact"
                            />
                            צור קשר
                        </Link>
                    </li>
                </ul>
            </nav>
        </aside>
    );
};

export default NavBar;
