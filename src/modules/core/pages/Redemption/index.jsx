import React, { Component } from 'react';
import Table from '../../../common/components/ReactTable';

import './style.scss';

class Redemption extends Component {

    render() {
        const redemptionList = [
            {
                couponCode: 4254,
                couponName: '10 שח הנחה על עוף',
                clientNum: 12356,
                redemptionDate: '16/9/23',
                city: 'תל אביב',
                redemptonAdress: 'בגין 35, תל אביב',
            },
            {
                couponCode: 587,
                couponName: 'קנה שמפו דאב קבל אחד נוסף חינם',
                clientNum: 12456,
                redemptionDate: '16/9/23',
                city: 'חיפה',
                redemptonAdress: 'בגין 35, תל אביב',
            },
            {
                couponCode: 258,
                couponName: '5 ש"ח הנחה על שוקולד למריחה נוטלה',
                clientNum: 12345,
                redemptionDate: '16/9/23',
                city: 'ירושלים',
                redemptionAdress: 'בגין 35, תל אביב',
            },
        ];
        const columns = [
            { Header: 'קוד הקופון', accessor: 'couponCode' },
            { Header: 'שם הקופון', accessor: 'couponName' },
            { Header: 'מספר הלקוח', accessor: 'clientNum' },
            { Header: 'תאריך המימוש', accessor: 'redemptionDate' },
            { Header: 'עיר המימוש', accessor: 'city' },
            { Header: 'כתובת המימוש', accessor: 'redemptionAdress' },
        ];

        return (
            <div className="page-align">
                <div className="top-bar">
                    <img
                        src="../../../../assets/svg/Asset 43.png"
                        alt="redemption-png"
                    />
                    <h1 className="top-bar-title">מימושים</h1>
                </div>
                <div className="pad-page">
                    <div
                        className="section"
                        styleName="redemption-filter-section"
                    >
                        <div className="bold size-16 mb-10">סינון לפי:</div>
                        <input
                            styleName="redemption-free-search"
                            type="text"
                            name=""
                            placeholder="הכנס טקסט חופשי"
                        />
                        <div>
                            <div className="flex row mt-25">
                                <div className="ml-25 flex col space-between">
                                    <h1 className="bold">מיקום המימוש</h1>
                                    <input
                                        styleName="redemption-location-input"
                                        type="text"
                                        name=""
                                        placeholder="הכנס מחוז או עיר"
                                        id=""
                                    />
                                </div>
                                <div className="ml-25">
                                    <h1 className="bold">תאריך המימוש</h1>
                                    <div className="flex row">
                                        <div styleName="col-gray">
                                            החל מ
                                            <div className="ml-25">
                                                <input
                                                    styleName="redemption-date-input"
                                                    type="date"
                                                />
                                            </div>
                                        </div>
                                        <div styleName="col-gray">
                                            עד
                                            <div>
                                                <input
                                                    styleName="redemption-date-input"
                                                    type="date"
                                                />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div className="align-self-end">
                                    <button styleName="filter-btn">
                                        סנון נתונים
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div>
                        <div className="title-bar flex space-between">
                            <h1 className="">
                                טבלת הלקוחות
                            </h1>
                            <div styleName="list-options">
                                <img
                                    src="../../../../assets/svg/Asset 28.png"
                                    alt="download"
                                />
                                <img
                                    src="../../../../assets/svg/Asset 26.png"
                                    alt="share"
                                />
                                <img
                                    src="../../../../assets/svg/Asset 18.png"
                                    alt="maximize"
                                />
                            </div>
                        </div>
                        <Table data={redemptionList} columns={columns} />
                    </div>
                </div>
            </div>
        );
    }

}

export default Redemption;
