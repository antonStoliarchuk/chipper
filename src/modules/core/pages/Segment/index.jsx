import React from 'react';
import { Link } from 'react-router';
import SegmentList from './SegmentList';

import './style.scss';

const Segment = () => {
    return (
        <div className="page-align">
            <div className="top-bar">
                <div className="flex align-center">
                    <img
                        src="../../../../assets/svg/Asset 42.png"
                        alt="segment-png"
                    />
                    <h1 className="top-bar-title ">סגמנטים</h1>
                </div>
                <div className="mr-25">
                    <Link to="/">
                        <img
                            src="../../../../assets/svg/Asset 63.png"
                            alt="segment-png"
                        />
                        <span className="spacer" />
                        צור סגמנט לקוחות
                    </Link>
                </div>
            </div>
            <div className="pad-page">
                <div className="flex space-between" styleName="top-segment-bar">
                    <div styleName="right-side-options">
                        <button className="ml-25">
                            שתף{' '}
                            <img
                                styleName="share-icon"
                                src="../../../../assets/svg/Asset 26.png"
                                alt="share-png"
                            />
                        </button>
                        <button className="">צור קופון ע"ב הבחירה</button>
                        <a href="/" className="mr-40">
                            בחר הכל
                        </a>
                    </div>
                    <div styleName="left-side-options">
                        <button>
                            <img
                                src="../../../../assets/svg/Asset 22.png"
                                alt="Excel"
                            />
                            <span>הורדת הסגמנט</span>
                            <img
                                src="../../../../assets/svg/Asset 16.png"
                                alt="download"
                                styleName="dark"
                            />
                        </button>
                        <a href="/" className="mr-30">
                            <img
                                src="../../../../assets/svg/Asset 23.png"
                                alt="delete"
                            />
                        </a>
                    </div>
                </div>
                <div className="pad-y-20" styleName="segment-filter">
                    <div className="bold size-20 mb-15">מיון סגמנטים</div>
                    <div className="bold size-16 mb-15">מיין לפי:</div>
                    <div className="size-16">
                        <Link to="/">
                            <span className="bold">הפעילים ביותר</span>
                        </Link>
                        <span styleName="seperator">|</span>
                        <Link to="/">האחרונים שנוצרו</Link>
                        <span styleName="seperator">|</span>
                        <Link to="/">כמות משתמשים כוללת</Link>
                    </div>
                </div>
                <div className="flex wrap space-between mt-45">
                    <SegmentList />
                    <SegmentList />
                    <SegmentList />
                    <SegmentList />
                    <SegmentList />
                    <SegmentList />
                    <SegmentList />
                </div>
            </div>
        </div>
    );
};

export default Segment;
