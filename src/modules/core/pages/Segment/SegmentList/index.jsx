import React from 'react';

import './style.scss';

const SegmentList = () => {
    return (
        <div className="flex col space-around" styleName="segment-item">
            <div>
                <button>
                    <img
                        src="../../../../assets/svg/Asset 18.png"
                        alt="maximize"
                        styleName="segment-maximize"
                    />
                </button>
                <input
                    type="checkbox"
                    name="segment_id"
                    id=""
                    styleName="segment-checkbox"
                />
                <div styleName="segment-title">
                    <h4 className="bold size-16">שם</h4>
                    <p className="bold size-20">גברים, תל-אביב, 25-32</p>
                </div>
            </div>
            <div className="flex mr-20">
                <div>
                    <div className="bold size-16">נוצר בתאריך</div>
                    <div className="bold size-20">17.8.17</div>
                </div>
                <div className="mr-50">
                    <div className="bold size-16">כמות לקוחות</div>
                    <div className="bold size-20">2,578</div>
                </div>
            </div>
        </div>
    );
};

export default SegmentList;
