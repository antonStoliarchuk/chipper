export Error from './Error';
export Home from './Home';
export NotFound from './NotFound';
export DashBoardMain from './DashboardMain';
export Segment from './Segment';
export Client from './Client';
export Redemption from './Redemption';
export Coupon from './Coupon';
export Campaign from './Campaign';
export Report from './Report';
export Branch from './Branch';
export PersonalArea from './PersonalArea';
