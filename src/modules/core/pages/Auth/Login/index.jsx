import React, { Component } from 'react';
// import api from '@/helpers/api';
import { connect } from 'quick-redux';

import './style.scss';

@connect((state, ownProps, actions) => ({
    auth: state.auth,
    actions: {
        login: actions.auth.login,
    },
}))
class Login extends Component {

    handleSubmit = async (event) => {
        event.preventDefault();

        const { actions } = this.props;

        const response = await actions.login({
            email: '12345711',
            password: '3',
        });

        console.log(response);
    }
    render() {
        return (
            <div className="pad-page page-align text-center">
                <div className="flex col">
                    <h1>Login page</h1>
                    <form action="POST" onSubmit={this.handleSubmit}>
                        <input
                            type="text"
                            name="user"
                            placeholder="user name"
                            styleName="login-input"
                        />
                        <br />
                        <input
                            type="text"
                            name="password"
                            placeholder="password"
                            styleName="login-input"
                        />
                        <br />
                        <button type="submit" styleName="submit">
                            Go
                        </button>
                    </form>
                </div>
            </div>
        );
    }

}

export default Login;
