import localStorageService from '@/services/LocalStorage/';

const authReducer = {
    key: 'auth',
    defaultState: {
        user: {},
    },
    actions: {
    },
    asyncActions: {
        async login({ actions, api }, data) {
            const response = await api.login(data);
            localStorageService.saveUser(response.data);
        },
    },
};

export default authReducer;
