import React, { Component } from 'react';
import CheckBox from '../../../forms/components/InputCheckbox';
import Table from '../../../common/components/ReactTable';

import './style.scss';

class Branch extends Component {

    handleChange = (e) => {
        e.persist();
        console.log(e.target.name);
    }

    render() {

        const branchList = [
            { branchNumber: '123', branchName: 'סניף קריית אליעזר', branchAdress: 'חיפה פל ים', branchPhone: '123-456-789', branchMail: 'a@a.com', openingHourse: 'א-ה 8:00-20:00 ו 07:30 - 19:00 ש` 19:00 - 23:00' },
            { branchNumber: '123', branchName: 'סניף קריית אליעזר', branchAdress: 'חיפה פל ים', branchPhone: '123-456-789', branchMail: 'a@a.com', openingHourse: 'א-ה 8:00-20:00 ו 07:30 - 19:00 ש` 19:00 - 23:00' },
            { branchNumber: '123', branchName: 'סניף קריית אליעזר', branchAdress: 'חיפה פל ים', branchPhone: '123-456-789', branchMail: 'a@a.com', openingHourse: 'א-ה 8:00-20:00 ו 07:30 - 19:00 ש` 19:00 - 23:00' },
            { branchNumber: '123', branchName: 'סניף קריית אליעזר', branchAdress: 'חיפה פל ים', branchPhone: '123-456-789', branchMail: 'a@a.com', openingHourse: 'א-ה 8:00-20:00 ו 07:30 - 19:00 ש` 19:00 - 23:00' },
        ];

        const branchColumns = [
            { Header: 'מספר סניף', accessor: 'branchNumber' },
            { Header: 'שם סניף', accessor: 'branchName' },
            { Header: 'כתובת', accessor: 'branchAdress' },
            { Header: 'טלפון', accessor: 'branchPhone' },
            { Header: 'דוא"ל', accessor: 'branchMail' },
            { Header: 'שעות פתיחה', accessor: 'openingHourse' },
        ];

        return (
            <div className="page-align">
                <div className="top-bar">
                    <img
                        src="../../../../assets/svg/Asset 51.png"
                        alt="report"
                    />
                    <h1 className="top-bar-title">סניפים</h1>
                </div>
                <div className="pad-page">
                    <div className="section pad-20 relative">
                        <button styleName="upload-btn input-height">
                            <span>העלאת סניף</span>
                            <img src="../../../../assets/svg/Asset 12.png" alt="upload" />
                        </button>
                        <div className="flex">
                            <div className="flex col ml-20">
                                <h1 className="bold">שם הסניף</h1>
                                <input type="text" name="" id="" styleName="branch-name input-height" />
                            </div>
                            <div className="flex col ml-20">
                                <h1 className="bold">טלפון</h1>
                                <input type="number" name="" id="" styleName="input-field-width input-height" />
                            </div>
                            <div className="flex col">
                                <h1 className="bold">דוא"ל</h1>
                                <input type="email" name="" id="" styleName="input-field-width input-height" />
                            </div>
                        </div>
                        <div className="flex mt-45">
                            <div className="flex col ml-20">
                                <h1 className="bold">מספר הסניף</h1>
                                <input type="text" name="" id="" styleName="input-field-width input-height" />
                            </div>
                            <div className="flex col ml-20">
                                <h1 className="bold">עיר</h1>
                                <input type="number" name="" id="" styleName="input-field-width input-height" />
                            </div>
                            <div className="flex col ml-20">
                                <h1 className="bold">כתובת</h1>
                                <input type="email" name="" id="" styleName="input-field-width input-height" />
                            </div>
                            <div className="flex col">
                                <h1 className="bold">מיקוד</h1>
                                <input type="number" name="" id="" styleName="input-field-width input-height" />
                            </div>
                        </div>
                        <div className="flex mt-45">
                            <div className="flex col justify-center">
                                <h1 className="bold">שעות פתיחה</h1>
                                <div className="flex wrap">
                                    <CheckBox name="sun" dayName="ראשון" onChange={this.handleChange} />
                                    <CheckBox name="mon" dayName="שני" onChange={this.handleChange} />
                                    <CheckBox name="tue" dayName="שלישי" onChange={this.handleChange} />
                                    <CheckBox name="wed" dayName="רביעי" onChange={this.handleChange} />
                                    <CheckBox name="thu" dayName="חמישי" onChange={this.handleChange} />
                                    <CheckBox name="fri" dayName="שישי" onChange={this.handleChange} />
                                    <CheckBox name="sat" dayName="שבת" onChange={this.handleChange} />
                                </div>
                            </div>
                            <div className="flex">
                                <div className="flex col ml-20">
                                    <h1 className="bold">מ</h1>
                                    <input type="number" name="" id="" styleName="input-range input-height" />
                                </div>
                                <div className="flex col ml-20">
                                    <h1 className="bold">עד</h1>
                                    <input type="number" name="" id="" styleName="input-range input-height" />
                                </div>
                                <div className="align-self-end">
                                    <button styleName="add-branch-btn">הוסף</button>
                                </div>
                            </div>
                        </div>
                        <div className="flex mt-20">
                            <div className="flex col ml-130">
                                <div className="flex">
                                    <button>
                                        <img src="../../../../assets/svg/Asset 24.png" alt="delete" />
                                    </button>
                                    <div className="pad-right-10">
                                        <h2>ראשון, שני, שלישי, רביעי, חמישי - 20:00 - 08:00</h2>
                                    </div>
                                </div>
                                <div className="flex mt-20">
                                    <button>
                                        <img src="../../../../assets/svg/Asset 24.png" alt="delete" />
                                    </button>
                                    <div className="pad-right-10">
                                        <h2>שישי - 19:00 - 07:30</h2>
                                    </div>
                                </div>
                            </div>
                            <div className="flex col">
                                <h1 className="bold">הוספת הערת חגים ומועדים</h1>
                                <textarea name="" id="" cols="59" rows="4" styleName="text-box" defaultValue="הוסף הערה" />
                            </div>
                        </div>
                    </div>
                    <div className="flex col section-border">
                        <div className="title-bar flex space-between">
                            <h1 className="bold size-20">טבלת הסניפים</h1>
                            <div className="icon-group">
                                <img src="../../../../assets/svg/Asset 16.png" alt="download" />
                                <img src="../../../../assets/svg/Asset 17.png" alt="share" />
                                <img src="../../../../assets/svg/Asset 18.png" alt="maximize" />
                            </div>
                        </div>
                        <Table data={branchList} columns={branchColumns} />
                    </div>
                </div>
            </div>
        );
    }

}

export default Branch;
