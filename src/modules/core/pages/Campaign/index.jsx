import React, { Component } from 'react';
import Table from '../../../common/components/ReactTable';

import './style.scss';

class Campaign extends Component {

    render() {
        const campaignList = [
            {
                campiagnName: 'קמפיין 123',
                createdAt: '16/9/23',
                expireAt: '16/9/23',
                redemptionCount: '15,485',
                status: 'פעיל',
            },
            {
                campiagnName: 'קמפיין 123',
                createdAt: '16/9/23',
                expireAt: '16/9/23',
                redemptionCount: '15,485',
                status: 'פעיל',
            }, {
                campiagnName: 'קמפיין 123',
                createdAt: '16/9/23',
                expireAt: '16/9/23',
                redemptionCount: '15,485',
                status: 'פעיל',
            }, {
                campiagnName: 'קמפיין 123',
                createdAt: '16/9/23',
                expireAt: '16/9/23',
                redemptionCount: '15,485',
                status: 'פעיל',
            },
        ];
        const columns = [
            {
                Header: 'שם הקמפיין',
                accessor: 'campiagnName',
            },
            {
                Header: 'תאריך היצירה',
                accessor: 'createdAt',
            },
            {
                Header: 'תאריך התפוגה',
                accessor: 'expireAt',
            },
            {
                Header: 'כמות המימושים',
                accessor: 'redemptionCount',
            },
            {
                Header: 'סטאטוס',
                accessor: 'status',
            },
        ];
        return (
            <div className="page-align">
                <div className="top-bar">
                    <div className="flex align-center">
                        <img
                            src="../../../../assets/svg/Asset 47.png"
                            alt="campaign"
                        />
                        <h1 className="top-bar-title">קמפיינים</h1>
                    </div>
                </div>
                <div className="pad-page">
                    <div className="section flex col">
                        <div className="flex pad-20">
                            <div className="bold size-16 ml-25">
                                <h3 className="mb-10">סינון לפי</h3>
                                <input
                                    type="text"
                                    name=""
                                    placeholder="הכנס טקסט חופשי"
                                    styleName="filter-by"
                                />
                            </div>
                            <div className="bold size-16">
                                <h3 className="mb-10 no-select">סטאטוס</h3>
                                <span className="no-select">פעיל</span>
                                <input
                                    styleName="checkbox-hack"
                                    className="no-select"
                                    type="checkbox"
                                    name="active"
                                    id="checkbox-toggle"
                                    onClick={(e) => this.toggleActiveStatus(e)}
                                />
                                <label
                                    styleName="checkbox-toggle"
                                    htmlFor="checkbox-toggle"
                                />
                                <span className="no-select">סגור</span>
                            </div>
                        </div>
                        <div className="pad-20">
                            <div className="flex">
                                <div>
                                    <h3 className="mb-10 bold size-16">
                                        כמות המימושים
                                    </h3>
                                    <div className="flex">
                                        <div className="ml-25">
                                            <h4 className="size-14 reg">
                                                החל מ
                                            </h4>
                                            <input
                                                styleName="campiagn-filter-input"
                                                type="number"
                                                name=""
                                            />
                                        </div>
                                        <div>
                                            <h4 className="size-14 reg">עד</h4>
                                            <input
                                                styleName="campiagn-filter-input"
                                                type="number"
                                                name=""
                                            />
                                        </div>
                                    </div>
                                </div>
                                <div className="mr-25 ml-25">
                                    <h3 className="mb-10 bold size-16">
                                        תאריך יצירה
                                    </h3>
                                    <div className="flex">
                                        <div className="ml-25">
                                            <h4 className="size-14 reg">
                                                החל מ
                                            </h4>
                                            <input
                                                styleName="campiagn-filter-input"
                                                type="date"
                                                name=""
                                            />
                                        </div>
                                        <div>
                                            <h4 className="size-14 reg">עד</h4>
                                            <input
                                                styleName="campiagn-filter-input"
                                                type="date"
                                                name=""
                                            />
                                        </div>
                                    </div>
                                </div>
                                <div className="ml-20">
                                    <h3 className="mb-10 bold size-16">
                                        תאריך תפוגה
                                    </h3>
                                    <div className="flex">
                                        <div className="ml-25">
                                            <h4 className="size-14 reg">
                                                החל מ
                                            </h4>
                                            <input
                                                styleName="campiagn-filter-input"
                                                type="date"
                                                name=""
                                            />
                                        </div>
                                        <div>
                                            <h4 className="size-14 reg">עד</h4>
                                            <input
                                                styleName="campiagn-filter-input"
                                                type="date"
                                                name=""
                                            />
                                        </div>
                                    </div>
                                </div>
                                <div className="align-self-end">
                                    <button styleName="filter-btn">
                                        סנון נתונים
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div>
                        <div className="title-bar flex space-between">
                            <h1 className="bold size-20">טבלאת הקמפיינים</h1>
                            <div styleName="list-options">
                                <img
                                    src="../../../../assets/svg/Asset 28.png"
                                    alt="download"
                                />
                                <img
                                    src="../../../../assets/svg/Asset 26.png"
                                    alt="share"
                                />
                                <img
                                    src="../../../../assets/svg/Asset 18.png"
                                    alt="maximize"
                                />
                            </div>
                        </div>
                        <Table data={campaignList} columns={columns} />
                    </div>
                </div>
            </div>
        );
    }

}

export default Campaign;
