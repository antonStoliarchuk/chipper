import React, { Component } from 'react';
import Table from '@/modules/common/components/ReactTable';
// import InputSelect from '@/modules/forms/components/InputSelect';

import './style.scss';

class Coupon extends Component {

    toggleActiveStatus(e) {
        e.persist();
        console.log('handling toggle', e.target.checked);
    }
    render() {
        const couponList = [
            {
                checkbox: '',
                couponID: '4254',
                couponType: 'כמותי',
                couponName: '10 ש"ח הנחה על עוף מעושן',
                campaignName: "'קמפיין א",
                redemptionCount: 14455,
                status: 'פעיל',
            },
            {
                checkbox: '',
                couponID: '587',
                couponType: 'יחסי',
                couponName: 'קנה שמפו דאב קבל שמפו וואטאוור',
                campaignName: "'קמפיין ב",
                redemptionCount: 12345,
                status: 'פעילך',
            },
            {
                checkbox: '',
                couponID: '256',
                couponType: 'כמותי',
                couponName: '5 ש"ח הנחה לשוקולד סכרת',
                campaignName: "'קמפיין א",
                redemptionCount: 8680,
                status: 'פעיל',
            },
            {
                checkbox: '',
                couponID: '3978',
                couponType: 'יחסי',
                couponName: 'הניה על שישית קוקה קוהל',
                campaignName: "'קמפיין ב",
                redemptionCount: 954,
                status: 'בהמתנה',
            },
        ];
        const columns = [
            {
                Header: '',
                accessor: 'checkbox',
            },
            {
                Header: 'קוד הקופון',
                accessor: 'couponId',
            },
            {
                Header: 'סוג הקופון',
                accessor: 'couponType',
            },
            {
                Header: 'שם הקופון',
                accessor: 'couponName',
            },
            {
                Header: 'שם הקמפיין',
                accessor: 'campaignName',
            },
            {
                Header: 'כמות המימושים',
                accessor: 'redemptionCount',
            },
            {
                Header: 'סטאטוס',
                accessor: 'status',
            },
        ];
        return (
            <div className="page-align">
                <div className="top-bar">
                    <div className="flex align-center">
                        <img
                            src="../../../../assets/svg/Asset 45.png"
                            alt="coupons"
                        />
                        <h1 className="top-bar-title">קופונים</h1>
                    </div>
                </div>
                <div className="pad-page">
                    <div className="section flex col">
                        <div className="flex pad-20">
                            <div className="bold size-16 ml-25">
                                <h3 className="mb-10">סינון לפי</h3>
                                <input
                                    type="text"
                                    name=""
                                    placeholder="הכנס טקסט חופשי"
                                    styleName="filter-by"
                                />
                            </div>
                            <div className="bold size-16 ml-25">
                                <h3 className="mb-10">סוג הקופון</h3>
                                <select name="" styleName="select-box">
                                    <option value="">Coupon type</option>
                                    <option value="">Coupon type</option>
                                    <option value="">Coupon type</option>
                                </select>
                            </div>
                            <div className="bold size-16">
                                <h3 className="mb-10 no-select">סטאטוס</h3>
                                <span>פעיל</span>
                                <input
                                    styleName="checkbox-hack"
                                    type="checkbox"
                                    name="active"
                                    id="checkbox-toggle"
                                    onClick={(e) => this.toggleActiveStatus(e)}
                                />
                                <label
                                    styleName="checkbox-toggle"
                                    htmlFor="checkbox-toggle"
                                />
                                <span>סגור</span>
                            </div>
                        </div>
                        <div className="pad-20">
                            <div className="flex">
                                <div className="ml-25">
                                    <h3 className="mb-10 bold size-16">
                                        כמות המימושים
                                    </h3>
                                    <div className="flex">
                                        <div className="ml-25">
                                            <h4 className="size-14 reg">
                                                החל מ
                                            </h4>
                                            <input
                                                styleName="coupon-redemption-start"
                                                type="number"
                                                name=""
                                            />
                                        </div>
                                        <div>
                                            <h4 className="size-14 reg">עד</h4>
                                            <input
                                                styleName="coupon-redemption-end"
                                                type="number"
                                                name=""
                                            />
                                        </div>
                                    </div>
                                </div>
                                <div className="ml-20">
                                    <h3 className="mb-10 bold size-16">
                                        תאריך תפוגה
                                    </h3>
                                    <div className="flex">
                                        <div className="ml-25">
                                            <h4 className="size-14 reg">
                                                החל מ
                                            </h4>
                                            <input
                                                styleName="coupon-redemption-start"
                                                type="date"
                                                name=""
                                            />
                                        </div>
                                        <div>
                                            <h4 className="size-14 reg">עד</h4>
                                            <input
                                                styleName="coupon-redemption-end"
                                                type="date"
                                                name=""
                                            />
                                        </div>
                                    </div>
                                </div>
                                <div className="align-self-end">
                                    <button styleName="filter-btn">
                                        סנון נתונים
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                    {/* <InputSelect
                        styleName="select-box"
                        placeholder="סוג הקופון"
                        options={[
                            { label: 'Coupon one', value: '1' },
                            { label: 'Coupon two', value: '2' }
                        ]}
                    /> */}
                    <div>
                        <div className="title-bar space-between">
                            <h1 className="bold size-20">טבלאת הקופונים</h1>
                            <div styleName="list-options">
                                <button styleName="create-campaign-btn">
                                    צור קמפיין
                                </button>
                                <img
                                    src="../../../../assets/svg/Asset 28.png"
                                    alt="download"
                                />
                                <img
                                    src="../../../../assets/svg/Asset 26.png"
                                    alt="share"
                                />
                                <img
                                    src="../../../../assets/svg/Asset 18.png"
                                    alt="maximize"
                                />
                            </div>
                        </div>
                    </div>
                    <Table data={couponList} columns={columns} />
                </div>
            </div>
        );
    }

}

export default Coupon;
