import React, { Component } from 'react';
import { Link } from 'react-router';
import './style.scss';

class PersonalArea extends Component {

    render() {
        return (
            <div className="page-align">
                <div className="top-bar">
                    <img
                        className="brighten"
                        src="../../../../assets/svg/Asset 30.png"
                        alt="report"
                    />
                    <h1 className="top-bar-title">אזור אישי</h1>
                </div>
                <div className="pad-page">
                    <div className="section">
                        <div className="title-bar">
                            <h1 className="bold size-20">פרטים אישיים</h1>
                            <Link to="edit-profile" className="bar-link mr-25">עדכן פרטים אישיים</Link>
                        </div>
                        <div className="flex pad-20">
                            <div className="flex col ml-25">
                                <h2 className="bold size-14">שם פרטי</h2>
                                <h1 className="size-20">שלומי</h1>
                            </div>
                            <div className="flex col ml-25">
                                <h2 className="bold size-14">שם משפחה</h2>
                                <h1 className="size-20">משולם</h1>
                            </div>
                            <div className="flex col ml-25">
                                <h2 className="bold size-14">אימייל</h2>
                                <h1 className="size-20">test@test.test</h1>
                            </div>
                            <Link to="edit-password" className="bar-link align-self-center">שינוי ססמא</Link>
                        </div>
                    </div>
                    <div className="section">
                        <div className="title-bar">
                            <h1 className="bold size-20">פרטי החברה</h1>
                        </div>
                        <div className="flex pad-20">
                            <div className="flex col  ml-130">
                                <h1 className="bold size-14">שם החברה</h1>
                                <h2 className="size-20">אדאקטיב</h2>
                            </div>
                            <div className="flex col ml-25">
                                <h1 className="bold size-14">טלפון החברה</h1>
                                <h2 className="size-20">123-1231231</h2>
                            </div>
                            <div className="flex col ml-25">
                                <h1 className="bold size-14">עיר</h1>
                                <h2 className="size-20">חולון</h2>
                            </div>
                            <div className="flex col ml-25">
                                <h1 className="bold size-14">רחוב</h1>
                                <h2 className="size-20">אריה שנקר</h2>
                            </div>
                            <div className="flex col ml-25">
                                <h1 className="bold size-14">מיקוד</h1>
                                <h2 className="size-20">352451</h2>
                            </div>
                        </div>
                        <div className="flex mt-45 pad-20">
                            <div className="flex col">
                                <h1 className="size-20">הגדרות התראה</h1>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }

}

export default PersonalArea;
