import React, { Component } from 'react';
import OpenDistRequest from '@/modules/core/components/openDistRequest';
import DateRangePicker from '@wojtekmaj/react-daterange-picker';
import Table from '@/modules/common/components/ReactTable';

import './style.scss';

class Report extends Component {

    state = {
        date: [new Date(), new Date()],
    }

    onDateChange = (date) => {
        console.log('changing date', date);
        this.setState({ date });
    }

    render() {
        const supplierList = [
            { supllierName: 'פרוקטור אנד גמבל', approveDate: '10.10.17', activeCoupons: 80, totalRevenue: '154,850' },
            { supllierName: 'נסטלה', approveDate: '10.10.17', activeCoupons: 35, totalRevenue: '301,850' },
            { supllierName: 'שטראוס', approveDate: '10.10.17', activeCoupons: 45, totalRevenue: '250,850' },
            { supllierName: 'יונילוור', approveDate: '10.10.17', activeCoupons: 25, totalRevenue: '200,850' },
            { supllierName: 'רקיט בנקיזר', approveDate: '10.10.17', activeCoupons: 23, totalRevenue: '115,850' },
            { supllierName: 'יפאורה', approveDate: '10.10.17', activeCoupons: 12, totalRevenue: '124,850' },
            { supllierName: 'יפאורה', approveDate: '10.10.17', activeCoupons: 12, totalRevenue: '124,850' },
            { supllierName: 'יפאורה', approveDate: '10.10.17', activeCoupons: 12, totalRevenue: '124,850' },
            { supllierName: 'יפאורה', approveDate: '10.10.17', activeCoupons: 12, totalRevenue: '124,850' },
            { supllierName: 'יפאורה', approveDate: '10.10.17', activeCoupons: 12, totalRevenue: '124,850' },
            { supllierName: 'יפאורה', approveDate: '10.10.17', activeCoupons: 12, totalRevenue: '124,850' },

        ];
        const supplierColumns = [
            { Header: 'שם היצרן/ספק', accessor: 'supllierName' },
            { Header: 'תאריך אישור', accessor: 'approveDate' },
            { Header: 'קופונים פעילים', accessor: 'activeCoupons' },
            { Header: 'סה"כ ערך מימושים', accessor: 'totalRevenue' },
        ];
        const supplierCoupons = [
            { copuponCode: 4254, couponType: 'כמותי', couponName: '10 שח הנחה', couponValue: '10 שח', redemptionCount: '15,587', totalValue: '154,580 שח' },
            { copuponCode: 4254, couponType: 'כמותי', couponName: '10 שח הנחה', couponValue: '10 שח', redemptionCount: '15,587', totalValue: '154,580 שח' },
            { copuponCode: 4254, couponType: 'כמותי', couponName: '10 שח הנחה', couponValue: '10 שח', redemptionCount: '15,587', totalValue: '154,580 שח' },
            { copuponCode: 4254, couponType: 'כמותי', couponName: '10 שח הנחה', couponValue: '10 שח', redemptionCount: '15,587', totalValue: '154,580 שח' },
        ];
        const supplierTitle = [
            { Header: 'קוד הקופון', accessor: 'copuponCode' },
            { Header: 'סוג הקופון', accessor: 'couponType' },
            { Header: 'שם הקופון', accessor: 'couponName' },
            { Header: 'ערך הקופון', accessor: 'couponValue' },
            { Header: 'כמות המימושים', accessor: 'redemptionCount' },
            { Header: 'סה"כ ערך', accessor: 'totalValue' },
        ];

        return (
            <div className="page-align" styleName="overflow-fix">
                <div className="top-bar flex space-between mb-10">
                    <div className="flex align-center">
                        <div className="flex align-center ml-25">
                            <img
                                src="../../../../assets/svg/Asset 49.png"
                                alt="report"
                            />
                            <h1 className="top-bar-title">
                                דו"ח מימושים
                            </h1>
                        </div>
                        <div className="ml-25" styleName="title-options">
                            <span className="ml-20">הורד הכל</span>
                            <img src="../../../../assets/svg/Asset 16.png" alt="download-all" />
                        </div>
                        <div styleName="title-options">
                            <span className="ml-20">שתף הכל</span>
                            <img src="../../../../assets/svg/Asset 17.png" alt="share-all" />
                        </div>
                    </div>
                    <div className="flex align-center">
                        <div className="ml-20">
                            טווח תאריכים לבדיקה
                        </div>
                        <div className="date-range" styleName="cal-icon">
                            <DateRangePicker
                                onChange={this.onDateChange}
                                value={this.state.date}
                                calendarIcon={null}
                                clearIcon={null}
                                locale="he-IL"
                            />
                        </div>
                    </div>
                </div>
                <div className="pad-page flex space-between" styleName="top-content">
                    <div className="ml-20 section-border" styleName="supplier-list">
                        <div className="title-bar">
                            <h1 className="bold size-20">הספקים והיצרנים שלי</h1>
                        </div>
                        <Table data={supplierList} columns={supplierColumns} />
                    </div>
                    <div className="flex col section-border" styleName="distributor-request">
                        <div className="title-bar">
                            <h1 className="bold size-20">בקשות אישור מפיץ פתוחות</h1>
                        </div>
                        <div className="flex col overflow-y-overlay" styleName="distributor-list">
                            <OpenDistRequest name="תלמה" date="12.01.18" />
                            <OpenDistRequest name="קוקה קולה" date="07.01.18" />
                            <OpenDistRequest name="קוקה קולה" date="07.01.18" />
                            <OpenDistRequest name="קוקה קולה" date="07.01.18" />
                            <OpenDistRequest name="קוקה קולה" date="07.01.18" />
                            <OpenDistRequest name="קוקה קולה" date="07.01.18" />
                        </div>
                    </div>
                </div>
                <div className="pad-page">
                    <div className="section-border">
                        <div className="title-bar mt-25 space-between">
                            <div className="flex">
                                <h1 className="bold size-20 ml-15">פרוקטור אנד גמבל</h1>
                                <h1 className="bold size-20">23/4/17 - 21/8/17</h1>
                            </div>
                            <div className="flex space-between">
                                <div style={{ marginLeft: '30px' }}>
                                    <button styleName="stop-supplier">הפסק עבודה מול היצרן</button>
                                </div>
                                <div styleName="supplier-icons">
                                    <img src="../../../../assets/svg/Asset 16.png" alt="download" />
                                    <img src="../../../../assets/svg/Asset 17.png" alt="share" />
                                    <img src="../../../../assets/svg/Asset 18.png" alt="maximize" />
                                </div>
                            </div>
                        </div>
                        <div className="flex pad-20" styleName="supplier-info">
                            <div className="flex col ml-50">
                                <h2 className="bold size-16">כמות קופונים פעילים</h2>
                                <span className="size-20">25</span>
                            </div>
                            <div className="flex col ml-50">
                                <h2 className="bold size-16">כמות מימושים כוללת</h2>
                                <span className="size-20">85,400</span>
                            </div>
                            <div className="flex col ml-50">
                                <h2 className="bold size-16">ערך כלל המימושים</h2>
                                <span className="size-20" styleName="nis">905,258</span>
                            </div>
                        </div>
                        <Table data={supplierCoupons} columns={supplierTitle} />
                    </div>
                </div>
            </div>
        );
    }

}

export default Report;
