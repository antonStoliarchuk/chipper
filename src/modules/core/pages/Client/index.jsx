import React from 'react';
import Table from '../../../common/components/ReactTable';

import './style.scss';

const Clients = () => {
    const clientList = [
        {
            clientID: 1,
            gender: '10 שח הנחה',
            birthDate: '21.8.85',
            country: 'תל אביב',
            join_at: '16/9/23',
            redemptions: 84,
        },
        {
            clientID: 2,
            gender: 'נקבה',
            birthDate: '21.8.85',
            country: 'חולון',
            join_at: '16/9/23',
            redemptions: 84,
        },
        {
            clientID: 3,
            gender: '10 שח הנחה',
            dateOfBirth: 12345,
            birthDate: '16/9/23',
            country: 'חיפה',
            join_at: 84,
        },
    ];

    const columns = [
        {
            Header: "מס' הלקוח",
            accessor: 'clientID',
        },
        {
            Header: 'מגדר',
            accessor: 'gender',
        },
        {
            Header: 'תאריך לידה',
            accessor: 'birthDate',
        },
        {
            Header: 'עיר מגורים',
            accessor: 'country',
        },
        {
            Header: 'תאריך הצטרפות',
            accessor: 'join_at',
        },
        {
            Header: 'כמות המימושים',
            accessor: 'clientID',
        },
    ];

    return (
        <div className="page-align">
            <div className="top-bar flex space-between">
                <div className="flex align-center">
                    <img
                        src="../../../../assets/svg/Asset 40.png"
                        alt="clients-png"
                    />
                    <h1 className="top-bar-title">לקוחות</h1>
                </div>
                <div>
                    טווח תאריכים לבדיקה
                    <input
                        styleName="date-input-field"
                        type="date"
                        name=""
                        id=""
                    />
                </div>
            </div>
            <div className="pad-page">
                <div
                    className="flex col space-between"
                    styleName="client-filter"
                >
                    <div>
                        <div className="bold mb-10">סינון לפי:</div>
                        <input
                            styleName="filter-input-field"
                            type="text"
                            name=""
                            placeholder="הכנס מס' לקוח או עיר מגורים"
                        />
                    </div>
                    <div styleName="client-filter-bottom">
                        <div className="flex row">
                            <div className="ml-25">
                                <span className="bold">תאריך הצטרפות</span>
                                <div className="flex row">
                                    <div className="ml-25" styleName="col-gray">
                                        החל מ
                                        <div>
                                            <input
                                                styleName="join-date"
                                                type="date"
                                            />
                                        </div>
                                    </div>
                                    <div styleName="col-gray">
                                        עד
                                        <div>
                                            <input
                                                styleName="join-date"
                                                type="date"
                                            />
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div className="ml-25">
                                <span className="bold">כמות מימושים</span>
                                <div className="flex row">
                                    <div styleName="col-gray">
                                        החל מ
                                        <div className="ml-25">
                                            <input
                                                styleName="redemption-ammount"
                                                type="number"
                                            />
                                        </div>
                                    </div>
                                    <div styleName="col-gray">
                                        עד
                                        <div>
                                            <input
                                                styleName="redemption-ammount"
                                                type="number"
                                            />
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div className="flex col space-between ml-25">
                                <span className="bold">סגמנט</span>
                                <div>
                                    <select
                                        name=""
                                        id=""
                                        styleName="segment-select"
                                    >
                                        <option value="" />
                                        <option value="">val</option>
                                        <option value="">val</option>
                                    </select>
                                </div>
                            </div>
                            <div className="align-self-end">
                                <button styleName="filter-btn">
                                    סנון נתונים
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
                {/* end of client filter */}
                <div>
                    <div className="title-bar flex space-between">
                        <h1 className="bold size-20">טבלת הלקוחות</h1>
                        <div styleName="list-options">
                            <img
                                src="../../../../assets/svg/Asset 28.png"
                                alt="download"
                            />
                            <img
                                src="../../../../assets/svg/Asset 26.png"
                                alt="share"
                            />
                            <img
                                src="../../../../assets/svg/Asset 18.png"
                                alt="maximize"
                            />
                        </div>
                    </div>
                </div>
                <Table data={clientList} columns={columns} />
            </div>
        </div>
    );
};

export default Clients;
