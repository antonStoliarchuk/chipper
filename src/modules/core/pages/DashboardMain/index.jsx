import React, { Component } from 'react';
import DateRangePicker from '@wojtekmaj/react-daterange-picker';

import './style.scss';

class DashboardMain extends Component {

    state = {
        date: [new Date(), new Date()],
    }

    onDateChange = (date) => {
        console.log('changing date', date);
        this.setState({ date });
    }
    // API for graphs
    // couponsRedemption
    // couponsRedemptionByBranches
    // couponsRedemptionByCategory
    // couponsRedemptionByCity
    // couponsRedemptionByTypeOfSale
    // couponsRedemptionByCampaign
    render() {

        return (

            <div className="page-align">
                <div className="top-bar space-between mb-15">
                    <div className="flex align-center">
                        <img
                            src="../../../../assets/svg/Asset 38.png"
                            alt="dashboard-icon"
                        />
                        <h1 className="top-bar-title">דאשבורד ראשי</h1>
                    </div>
                    <div className="flex">
                        <div className="flex ml-15 align-center">
                            <h3>סנן לפי קמעונאי\ סניף</h3>
                            <div>
                                <select styleName="dashboard-item select-box" name="">
                                    <option value="">הכל</option>
                                    <option value="">א</option>
                                    <option value="">ב</option>
                                    <option value="">ג</option>
                                </select>
                            </div>
                        </div>
                        <div className="flex align-center">
                            <h3 className="ml-15">טווח תאריכים לבדיקה</h3>
                            <div className="date-range" styleName="cal-icon">
                                <DateRangePicker
                                    onChange={this.onDateChange}
                                    value={this.state.date}
                                    calendarIcon={null}
                                    clearIcon={null}
                                />
                            </div>
                        </div>
                        {/* <input
                                styleName="dashboard-item date-time"
                                type="date"
                                name=""
                            /> */}
                    </div>
                    <div>
                        <div>
                            <img
                                title="שתף"
                                styleName="download-icon"
                                src="../../../assets/svg/Asset 16.png"
                                alt="dashboard-download"
                            />
                            <img
                                title="הורד"
                                styleName="share-icon"
                                src="../../../assets/svg/Asset 17.png"
                                alt="dashboard-share"
                            />
                        </div>
                    </div>
                </div>
                <div className="pad-page flex wrap">
                    <div
                        className="section"
                        styleName="graph-item"
                    >
                        <div className="title-bar flex space-between">
                            <div>
                                <h1 className="bold size-14">
                                    סך המימושים לקופונים פעילים
                                </h1>
                            </div>
                            <div styleName="graph-options">
                                <img src="../../../assets/svg/Asset 16.png" alt="download" />
                                <img src="../../../assets/svg/Asset 17.png" alt="share" />
                                <img src="../../../assets/svg/Asset 18.png" alt="maximize" />
                            </div>
                        </div>
                    </div>
                    <div
                        className="section"
                        styleName="graph-item"
                    >
                        <div className="title-bar flex space-between">
                            <div>
                                <h1 className="bold size-14">
                                    מימוש לפי קופונים
                                </h1>
                            </div>
                            <div styleName="graph-options">
                                <img src="../../../assets/svg/Asset 16.png" alt="download" />
                                <img src="../../../assets/svg/Asset 17.png" alt="share" />
                                <img src="../../../assets/svg/Asset 18.png" alt="maximize" />
                            </div>
                        </div>
                    </div>
                    <div
                        className="section"
                        styleName="graph-item"
                    >
                        <div className="title-bar flex space-between">
                            <div>
                                <h1 className="bold size-14">
                                    מימוש לפי סניף
                                </h1>
                            </div>
                            <div styleName="graph-options">
                                <img src="../../../assets/svg/Asset 16.png" alt="download" />
                                <img src="../../../assets/svg/Asset 17.png" alt="share" />
                                <img src="../../../assets/svg/Asset 18.png" alt="maximize" />
                            </div>
                        </div>
                    </div>
                    <div
                        className="section"
                        styleName="graph-item"
                    >
                        <div className="title-bar flex space-between">
                            <div>
                                <h1 className="bold size-14">
                                    מימוש לפי סוג ההנחה
                                </h1>
                            </div>
                            <div styleName="graph-options">
                                <img src="../../../assets/svg/Asset 16.png" alt="download" />
                                <img src="../../../assets/svg/Asset 17.png" alt="share" />
                                <img src="../../../assets/svg/Asset 18.png" alt="maximize" />
                            </div>
                        </div>
                    </div>
                    <div
                        className="section"
                        styleName="graph-item"
                    >
                        <div className="title-bar flex space-between">
                            <div>
                                <h1 className="bold size-14">
                                    מימוש לפי מיקום
                                </h1>
                            </div>
                            <div styleName="graph-options">
                                <button styleName="map-btn">לצפייה במצב מפה
                                    <img src="../../../assets/svg/Asset 19.png" alt="map" />
                                </button>
                                <img src="../../../assets/svg/Asset 16.png" alt="download" />
                                <img src="../../../assets/svg/Asset 17.png" alt="share" />
                                <img src="../../../assets/svg/Asset 18.png" alt="maximize" />
                            </div>
                        </div>
                    </div>
                    <div
                        className="section"
                        styleName="graph-item"
                    >
                        <div className="title-bar flex space-between">
                            <div>
                                <h1 className="bold size-14">
                                    מימוש לפי קטגורית המוצר
                                </h1>
                            </div>
                            <div styleName="graph-options">
                                <img src="../../../assets/svg/Asset 16.png" alt="download" />
                                <img src="../../../assets/svg/Asset 17.png" alt="share" />
                                <img src="../../../assets/svg/Asset 18.png" alt="maximize" />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }

}

export default DashboardMain;
